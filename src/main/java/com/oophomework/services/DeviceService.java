package main.java.com.oophomework.services;

import main.java.com.oophomework.models.Device;

public class DeviceService {
    public Device[] returnGreaterMemory(int memoryCapacity, Device [] devices){
        int devicesCount = 0;

        for (int i = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryLength() > memoryCapacity){
                devicesCount++;
            }
        }

        Device [] newDevices = new Device[devicesCount];

        for (int i = 0, j = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryLength() > memoryCapacity){
                newDevices[j] = devices[i];
                j++;
            }
        }

        return newDevices;
    }

    public Device[] returnLessMemory(int memoryCapacity, Device [] devices){
        int devicesCount = 0;

        for (int i = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryLength() < memoryCapacity){
                devicesCount++;
            }
        }

        Device [] newDevices = new Device[devicesCount];

        for (int i = 0, j = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryLength() < memoryCapacity){
                newDevices[j] = devices[i];
                j++;
            }
        }

        return newDevices;
    }

    public Device[] returnUseGreaterMemory(int occupiedMemory, Device [] devices){
        int devicesCount = 0;

        for (int i = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryOccupied() > occupiedMemory){
                devicesCount++;
            }
        }

        Device [] newDevices = new Device[devicesCount];

        for (int i = 0, j = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryOccupied() > occupiedMemory){
                newDevices[j] = devices[i];
                j++;
            }
        }

        return newDevices;
    }

    public Device[] returnUseLessMemory(int occupiedMemory, Device [] devices){
        int devicesCount = 0;

        for (int i = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryOccupied() < occupiedMemory){
                devicesCount++;
            }
        }

        Device [] newDevices = new Device[devicesCount];

        for (int i = 0, j = 0; i < devices.length; i++) {
            if(devices[i].memory.getMemoryOccupied() < occupiedMemory){
                newDevices[j] = devices[i];
                j++;
            }
        }

        return newDevices;
    }
}
