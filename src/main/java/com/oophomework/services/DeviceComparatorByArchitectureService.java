package main.java.com.oophomework.services;

import main.java.com.oophomework.models.Device;
import main.java.com.oophomework.models.Processor;

import java.util.Comparator;

public class DeviceComparatorByArchitectureService implements Comparator<Device> {
    @Override
    public int compare(Device o1, Device o2) {
        return o1.processor.getClass().getSimpleName().charAt(9) - o2.processor.getClass().getSimpleName().charAt(9);
    }
}
