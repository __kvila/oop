package main.java.com.oophomework.services;

import main.java.com.oophomework.models.Device;

import java.util.Comparator;

public class DeviceComparatorByProcessorOptions implements Comparator<Device> {
    @Override
    public int compare(Device o1, Device o2) {
        int result = o1.processor.getFrequency() - o2.processor.getFrequency();
        if(result == 0){
            result = o1.processor.getCache() - o2.processor.getCache();
            if(result == 0){
                return o1.processor.getBitCapacity() - o2.processor.getBitCapacity();
            }
            return result;
        }
        return result;
    }
}
