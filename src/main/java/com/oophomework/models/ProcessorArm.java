package main.java.com.oophomework.models;

public class ProcessorArm extends Processor{

    private static final String MSG_COMPLETED = "Successfully completed!";

    private static final String architecture = "ARM";

    @Override
    public String toString() {
        return "ProcessorArm{}";
    }

    @Override
    public String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        frequency = (int)data / 10;
        cache = (int)data / 5;
        bitCapacity = (int)data;

        return MSG_COMPLETED;
    }
}
