package main.java.com.oophomework.models;

import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;

public class Device {

    private static final String MSG_PROCESSOR = "Choose ARM or X86: ";
    private static final String MSG_INVALID_PROCESSOR_TYPE = "There is no such type. Select an existing!";

    public Processor processor;
    public Memory memory;

    public Device(String processorType, int processorOptions, int size){

            if (processorType.equalsIgnoreCase("ARM")) {
                processor = new ProcessorArm();
            } else if (processorType.equalsIgnoreCase("X86")) {
                processor = new ProcessorX86();
            }

        System.out.println("Enter the processor options");
        processor.dataProcess(processorOptions);
        memory = new Memory(size);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return Objects.equals(processor, device.processor) && Objects.equals(memory, device.memory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processor, memory);
    }

    @Override
    public String toString() {
        return "Device{" +
                "processor=" + processor.getClass().getSimpleName() +
                '}';
    }

    public void save(String [] data){
        for (int i = 0; i < data.length; i++) {
            memory.save(data[i]);
        }
    }

    public String [] readAll(){
        String [] returnableStrings = new String[memory.getMemoryLength()];
        for (int i = 0; i < memory.getMemoryLength(); i++) {
            returnableStrings[i] = memory.readLast();
            memory.removeLast();
        }

        return returnableStrings;
    }

    public void dataProcessing(){
        for (int i = 0; i < memory.getMemoryLength(); i++) {
                 memory.memoryCell[i] = processor.dataProcess(memory.memoryCell[i]);
            }
    }

    public String getSystemInfo(){
        return "Processor info:\n" + processor.getDetails() + "\n" + "Memory info:\n" + memory.getMemoryInfo();
    }
}
