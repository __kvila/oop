package main.java.com.oophomework.models;

import java.util.Objects;

public abstract class Processor {
    int frequency;
    int cache;
    int bitCapacity;

    @Override
    public String toString() {
        return "Processor{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Processor processor = (Processor) o;
        return frequency == processor.frequency && cache == processor.cache && bitCapacity == processor.bitCapacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(frequency, cache, bitCapacity);
    }

    public String getDetails(){
        return "Frequency: " + frequency + " | Cache: " + cache + " | Bit capacity: " + bitCapacity;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getCache() {
        return cache;
    }

    public int getBitCapacity() {
        return bitCapacity;
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);
}
