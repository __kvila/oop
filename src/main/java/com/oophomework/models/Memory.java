package main.java.com.oophomework.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Memory {

    private static final String MSG_ARRAY_IS_EMPTY = "Array is empty!";
    private static final String MSG_LAST_ELEMENT_DELETE = "Last element was deleted.";
    private static final String MSG_ENTER_VALUE = "Enter the value of the element...";

    private static final Scanner scanner = new Scanner(System.in);

    String [] memoryCell;

    public Memory(int size){

        memoryCell = new String [size];
        for (int i = 0; i < size; i++) {
            memoryCell[i] = null;
        }
    }

    public int getMemoryLength(){
        return memoryCell.length;
    }

    public String readLast(){
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if(memoryCell[i] != null){
                return memoryCell[i];
            }
        }
        return MSG_ARRAY_IS_EMPTY;
    }

    public String removeLast(){
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if(memoryCell[i] != null){
                memoryCell[i] = null;
                return MSG_LAST_ELEMENT_DELETE;
            }
        }
        return MSG_ARRAY_IS_EMPTY;
    }

    public boolean save(String strArr){
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if(memoryCell[i] == null){
                memoryCell[i] = strArr;
                return true;
            }
        }
        return false;
    }

    public int getMemoryOccupied(){
        int emptyCells = 0, memoryOccupied = 0;

        for (int i = 0; i < memoryCell.length; i++) {
            if(memoryCell[i] == null){
                emptyCells++;
            }
        }

        memoryOccupied = 100 / memoryCell.length * (memoryCell.length - emptyCells);

        return memoryOccupied;
    }

    public List<Integer> getMemoryInfo(){
        int emptyCells = 0, memoryOccupied = 0;
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < memoryCell.length; i++) {
            if(memoryCell[i] == null){
                emptyCells++;
            }
        }

        memoryOccupied = 100 / memoryCell.length * (memoryCell.length - emptyCells);

        list.add(memoryCell.length);
        list.add(memoryOccupied);
        return list;
    }
}
