package main.java.com.oophomework.models;

import java.util.Locale;

public class ProcessorX86 extends Processor{

    private static final String MSG_COMPLETED = "Successfully completed!";

    private static final String architecture = "X86";

    @Override
    public String toString() {
        return "ProcessorX86{}";
    }

    @Override
    public String dataProcess(String data) {
        return data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        frequency = (int)data / 10;
        cache = (int)data / 5;
        bitCapacity = (int)data;

        return MSG_COMPLETED;
    }
}
