package main.java.com.oophomework;

import main.java.com.oophomework.models.Device;
import main.java.com.oophomework.services.DeviceComparatorByArchitectureService;
import main.java.com.oophomework.services.DeviceComparatorByProcessorOptions;
import main.java.com.oophomework.services.DeviceService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ProjectMain {
    public static void main(String[] args) {

        DeviceService deviceService = new DeviceService();
        Device [] devices = new Device[10];

        devices[0] = new Device("ARM", 1024, 10);
        devices[1] = new Device("X86", 512, 5);
        devices[2] = new Device("X86", 256, 16);
        devices[3] = new Device("ARM", 2048, 8);
        devices[4] = new Device("X86", 1024, 12);
        devices[5] = new Device("ARM", 2048, 48);
        devices[6] = new Device("ARM", 1024, 2);
        devices[7] = new Device("ARM", 128, 4);
        devices[8] = new Device("X86", 512, 8);
        devices[9] = new Device("X86", 256, 10);

        Device [] devices1;
        //devices1 = deviceService.returnLessMemory(9, devices);
//
//        for (Device p:devices1) {
//            System.out.println(p.getSystemInfo());
//        }

        devices[0].memory.save("123");
        devices[0].memory.save("223");
        devices[1].memory.save("42");
        devices[1].memory.save("23");
        devices[1].memory.save("5675");
        devices[2].memory.save("634");
        devices[3].memory.save("64");
        devices[3].memory.save("12121");
        devices[3].memory.save("1");
        devices[4].memory.save("775");
        devices[5].memory.save("855");
        devices[5].memory.save("65");
        devices[6].memory.save("855");
        devices[6].memory.save("45");
        devices[7].memory.save("757");
        devices[7].memory.save("7");
        devices[7].memory.save("547");
        devices[7].memory.save("234");
        devices[9].memory.save("912");

        //devices1 = deviceService.returnUseGreaterMemory(20, devices);
        devices1 = deviceService.returnUseLessMemory(20, devices);

        for (Device p:devices1) {
            System.out.println(p.getSystemInfo());
        }

//        Device device1 = new Device("ARM", 1024, 10);
//        Device device2 = new Device("X86", 512, 5);
//        Device device3 = new Device("X86", 256, 16);
//        Device device4 = new Device("ARM", 2048, 8);
//        Device device5 = new Device("X86", 1024, 12);
//        Device device6 = new Device("ARM", 2048, 48);
//        Device device7 = new Device("ARM", 1024, 2);
//        Device device8 = new Device("ARM", 128, 4);
//        Device device9 = new Device("X86", 512, 8);
//        Device device10 = new Device("X86", 256, 10);
//
//        List<Device> devices = new ArrayList<>();
//
//        devices.add(device1);
//        devices.add(device2);
//        devices.add(device3);
//        devices.add(device4);
//        devices.add(device5);
//        devices.add(device6);
//        devices.add(device7);
//        devices.add(device8);
//        devices.add(device9);
//        devices.add(device10);
//
//        DeviceComparatorByArchitectureService deviceComparatorByArchitectureService = new DeviceComparatorByArchitectureService();
//        DeviceComparatorByProcessorOptions deviceComparatorByProcessorOptions = new DeviceComparatorByProcessorOptions();
//
//        devices.sort(deviceComparatorByProcessorOptions);
//
//        for (Device p:devices) {
//            System.out.println(p.getSystemInfo());
//        }

    }
}
